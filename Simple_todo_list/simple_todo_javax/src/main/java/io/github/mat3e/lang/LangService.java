package io.github.mat3e.lang;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
class LangService {

    private LangRepository langRepository;

    LangService(LangRepository langRepository) {
        this.langRepository = langRepository;
    }

    List<LangDTO> findAll() {
//        List<LangDTO> langDTOS = new ArrayList<>();
//        List<Lang> langs = langRepository.findAll();
//
//        for (Lang lang : langs) {
//            langDTOS.add(langMapper.toLangDTO(lang));
//        }
//
//        return langDTOS;

        return langRepository
                .findAll()
                .stream()
                .map(LangDTO::new)
                .collect(toList());
    }

}