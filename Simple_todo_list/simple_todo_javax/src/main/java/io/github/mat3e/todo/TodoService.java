package io.github.mat3e.todo;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
class TodoService {

    private TodoRepository todoRepository;

    TodoService(TodoRepository todoRepository) {
        this.todoRepository = todoRepository;
    }

    List<Todo> findAll() {
        return todoRepository.findAll();
    }


    Optional<Todo> toggleTodo(Integer id){
        var todo = todoRepository.findById(id);
        todo.ifPresent(t -> {
            t.setDone(!t.getDone());
            todoRepository.save(t);
        });
        return todo;
    }

    Todo addTodo(Todo todo){
        return todoRepository.save(todo);
    }

}